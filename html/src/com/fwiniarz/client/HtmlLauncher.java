package com.fwiniarz.client;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.gwt.GwtApplication;
import com.badlogic.gdx.backends.gwt.GwtApplicationConfiguration;
import com.fwiniarz.MazeHero;

public class HtmlLauncher extends GwtApplication {

        @Override
        public GwtApplicationConfiguration getConfig () {
                return new GwtApplicationConfiguration(428, 518);
        }

        @Override
        public ApplicationListener createApplicationListener () {
                return new MazeHero();
        }
}