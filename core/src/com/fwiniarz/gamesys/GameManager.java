package com.fwiniarz.gamesys;

import com.badlogic.gdx.ai.utils.Location;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Disposable;
import com.fwiniarz.ai.astar.AStartPathFinding;
import com.fwiniarz.components.PlayerComponent;
import com.fwiniarz.components.TreasureComponent;
import com.fwiniarz.constants.AssetConstants;
import com.fwiniarz.builders.WorldBuilder;
import com.fwiniarz.entities.TreasureEntity;
import com.fwiniarz.utils.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class GameManager implements Disposable {

    public static final GameManager instance = new GameManager();

    public static final float PPM = 64f;
    public static final long ROUND_TIME_MS = 300000; //300 000 ms = 5 min

    public static final short NOTHING_BIT = 0;
    public static final short WALL_BIT = 1;
    public static final short PLAYER_BIT = 1 << 1;
    public static final short TREASURE_BIT = 1 << 2;
    public static final short LIGHT_BIT = 1 << 4;

    public static final int PLAYER_COUNT = 3;

    public AssetManager assetManager;

    public WorldBuilder worldBuilder;

    public int player1Score = 0;
    public int player2Score = 0;
    public int player3Score = 0;

    public int player1DisplayScore = 0;
    public int player2DisplayScore = 0;
    public int player3DisplayScore = 0;

    public long gameStartTimestamp;
    public long gameElapsedTimestamp;
    public boolean playerIsAlive = true;
    private boolean gameOver = false;

    public AStartPathFinding pathfinder;

    private List<TreasureEntity> spawnedTreasures;
    private Vector2 player1Position;
    private Vector2 player2Position;
    private Vector2 player3Position;

    public boolean player1Alive;
    public boolean player2Alive;
    public boolean player3Alive;

    private GameManager() {
        assetManager = new AssetManager();
        assetManager.load("core/assets/images/" + AssetConstants.SPRITE_PACK, TextureAtlas.class);
        assetManager.load("core/assets/sounds/treasure_pickup.ogg", Sound.class);
        assetManager.load("core/assets/sounds/clear.ogg", Sound.class);

        assetManager.finishLoading();

        player1Position = new Vector2();
        player2Position = new Vector2();
        player3Position = new Vector2();
    }

    public void makeGameOver() {
        gameOver = true;
    }

    public boolean isGameOver() {
        return gameOver;
    }

    public void resetGame() {
        player1Score = 0;
        player2Score = 0;
        player3Score = 0;
        player1DisplayScore = 0;
        player2DisplayScore = 0;
        player3DisplayScore = 0;
        gameElapsedTimestamp = 0;
        player1Position = new Vector2();
        player2Position = new Vector2();
        player3Position = new Vector2();
        spawnedTreasures = new ArrayList<>();

        playerIsAlive = true;
        gameOver = false;
    }

    public void addScore(PlayerComponent.Type scoreForPlayer, int score) {

        if(scoreForPlayer != null) {
            switch (scoreForPlayer) {
                case PLAYER_1:
                    this.player1Score += score;
                    break;
                case PLAYER_2:
                    this.player2Score += score;
                    break;
                case PLAYER_3:
                    this.player3Score += score;
            }
        }
    }

    @Override
    public void dispose() {
        assetManager.dispose();
    }

    public List<TreasureEntity> getSpawnedTreasures() {
        return spawnedTreasures;
    }

    public void addSpawnedTreasure(TreasureComponent treasure, Vector2 location) {
        if(spawnedTreasures == null) {
            spawnedTreasures = new ArrayList<>();
        }

        spawnedTreasures.add(new TreasureEntity(treasure, location));
    }

    public void removeSpawnedTreasure(TreasureComponent treasure) {
        if(spawnedTreasures != null) {
            int deleteIndex = -1;
            int currentIndex = 0;

            for(TreasureEntity spawnedTreasure : spawnedTreasures) {
                if(spawnedTreasure.getTreasure().id == treasure.id) {
                    deleteIndex = currentIndex;
                }
                currentIndex++;
            }

            if(deleteIndex != -1) {
                spawnedTreasures.remove(deleteIndex);
            }
        }
    }

    public List<PlayerComponent> getPlayers() {
        return worldBuilder.getPlayers();
    }

    public PlayerComponent getPlayer(PlayerComponent.Type playerType) {
        PlayerComponent player = null;
        for(PlayerComponent playerComponent : getPlayers()) {
            if(playerComponent.playerType == playerType) {
                player = playerComponent;
                break;
            }
        }

        return player;
    }

    public void setPlayerPosition(PlayerComponent.Type player, Vector2 position) {
        switch (player) {
            case PLAYER_1:
                player1Position.x = position.x;
                player1Position.y = position.y;
                break;
            case PLAYER_2:
                player2Position.x = position.x;
                player2Position.y = position.y;
                break;
            case PLAYER_3:
                player3Position.x = position.x;
                player3Position.y = position.y;
                break;
        }
    }

    public Vector2 getPlayer1Position() {
        return player1Position;
    }

    public Vector2 getPlayer2Position() {
        return player2Position;
    }

    public Vector2 getPlayer3Position() {
        return player3Position;
    }

    public PlayerComponent.Type getWinner() {
        PlayerComponent.Type winner;
        List<PlayerComponent> players = getPlayers();

        if(players.size() == 1) {
            winner = players.get(0).playerType;
        } else {
            if(player1Score > player2Score && player1Score > player3Score) winner = PlayerComponent.Type.PLAYER_1;
            else if(player2Score > player1Score && player2Score > player3Score) winner = PlayerComponent.Type.PLAYER_2;
            else if(player3Score > player1Score && player3Score > player2Score) winner = PlayerComponent.Type.PLAYER_3;
            else winner = null;
        }

        return winner;
    }

    public void transferPoints(PlayerComponent.Type from, PlayerComponent.Type to) {
        int pointsToTransfer = (from == PlayerComponent.Type.PLAYER_1 ? player1Score
                                                                      : from == PlayerComponent.Type.PLAYER_2 ? player2Score
                                                                                                              : player3Score);

        switch(to) {
            case PLAYER_1:
                player1Score += pointsToTransfer;
                break;
            case PLAYER_2:
                player2Score += pointsToTransfer;
                break;
            case PLAYER_3:
                player3Score += pointsToTransfer;
                break;
        }

        Logger.logLineStandard("Transfering " + pointsToTransfer + " points from " + from + " to " + to);
    }
}
