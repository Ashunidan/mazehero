package com.fwiniarz.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.physics.box2d.Body;
import com.fwiniarz.components.MovementComponent;
import com.fwiniarz.components.TreasureComponent;
import com.fwiniarz.components.TextureComponent;
import com.fwiniarz.gamesys.GameManager;
import com.fwiniarz.utils.GlobalRandom;


public class TreasureSystem extends IteratingSystem {
    private final ComponentMapper<TreasureComponent> treasureM = ComponentMapper.getFor(TreasureComponent.class);
    private final ComponentMapper<MovementComponent> movementM = ComponentMapper.getFor(MovementComponent.class);
    private final ComponentMapper<TextureComponent> textureM = ComponentMapper.getFor(TextureComponent.class);

    private float timeAccumulator = 0f;

    public TreasureSystem() {
        super(Family.all(TreasureComponent.class, MovementComponent.class, TextureComponent.class).get());
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        timeAccumulator += deltaTime;
        TreasureComponent treasure = treasureM.get(entity);
        MovementComponent movement = movementM.get(entity);
        TextureComponent texture = textureM.get(entity);
        Body body = movement.body;

        if(!treasure.exists) {
            texture.show(false);
        }

        if (treasure.exists && treasure.taken) {
            treasure.collect(treasure.takenBy);
            GameManager.instance.addScore(treasure.takenBy, treasure.amount);
            GameManager.instance.assetManager.get("core/assets/sounds/treasure_pickup.ogg", Sound.class).play();
        }

        if(!treasure.exists && timeAccumulator >= 500.0f && !GameManager.instance.isGameOver()) {
            timeAccumulator = 0;
            treasure.spawn((GlobalRandom.getInstance().nextInt(5) + 1) * 100);
            texture.show(true);
            GameManager.instance.addSpawnedTreasure(treasure, body.getPosition());
        }
    }
}
