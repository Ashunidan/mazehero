package com.fwiniarz.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.fwiniarz.components.MovementComponent;
import com.fwiniarz.components.PlayerComponent;
import com.fwiniarz.components.StateComponent;
import com.fwiniarz.gamesys.GameManager;
import com.fwiniarz.utils.Logger;

public class PlayerSystem extends IteratingSystem {

    private final ComponentMapper<PlayerComponent> playerM = ComponentMapper.getFor(PlayerComponent.class);
    private final ComponentMapper<StateComponent> stateM = ComponentMapper.getFor(StateComponent.class);

    private float delayDeathTimer;

    public PlayerSystem() {
        super(Family.all(PlayerComponent.class, MovementComponent.class, StateComponent.class).get());
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        PlayerComponent player = playerM.get(entity);
        StateComponent state = stateM.get(entity);
        player.playerAgent.update(deltaTime);
        state.setState(player.getCurrentState());
        updateLocation(player);

        if(player.hp <= 0) deletePlayer(entity, player, deltaTime);
        if(player.nearbyPlayer != null && player.nearbyPlayer.hp <= 0) player.nearbyPlayer = null;

        checkFightDistance(player);
        player.regenerateHp(deltaTime);
    }

    private void updateLocation(PlayerComponent player) {
        GameManager.instance.setPlayerPosition(player.playerType, player.getBody().getPosition());
    }

    private void checkFightDistance(PlayerComponent player) {
        if(player.nearbyPlayer != null) {
            float distanceDiffX = Math.abs(player.getBody().getPosition().x - player.nearbyPlayer.getBody().getPosition().x);
            float distanceDiffY = Math.abs(player.getBody().getPosition().y - player.nearbyPlayer.getBody().getPosition().y);

            if(distanceDiffX > 2 || distanceDiffY > 2) {
                player.nearbyPlayer = null;
            }
        }
    }

    private void deletePlayer(Entity entity, PlayerComponent component, float deltaTime) {
        delayDeathTimer += deltaTime;

        if(delayDeathTimer >= 0.5f) {
            delayDeathTimer = 0;
            component.hp = 0;
            component.getBody().getWorld().destroyBody(component.getBody());
            getEngine().removeEntity(entity);

            component.markPlayerDead();
            GameManager.instance.transferPoints(component.playerType, component.nearbyPlayer.playerType);
        }
    }

}
