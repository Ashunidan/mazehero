package com.fwiniarz.builders;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import com.fwiniarz.components.AnimationComponent;
import com.fwiniarz.components.PlayerComponent;
import com.fwiniarz.constants.AssetConstants;
import com.fwiniarz.constants.Sizes;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.fwiniarz.utils.Logger;
import com.fwiniarz.utils.enums.StateEnum;

/**
 * MazeHero. Created by Filip on 25.08.2018.
 */
@SuppressWarnings("PointlessArithmeticExpression")
class AnimationBuilder {

    public static final int SOUL_WIDTH = 37;
    public static final String SOUL_REGION = "Soul";

    AnimationComponent buildPlayer1Animations(TextureAtlas actorAtlas) {
        
        AnimationComponent animationComponent = new AnimationComponent();
        Animation animation;
        Array<TextureRegion> keyFrames = new Array<>();
        keyFrames.clear();

        // move
        for (int i = 1; i < 8; i++) {
            keyFrames.add(new TextureRegion(actorAtlas.findRegion(AssetConstants.HERO1_SPRITE), i * (int)Sizes.HERO_WIDTH, 2 * (int)Sizes.HERO_HEIGHT, (int)Sizes.HERO_WIDTH, (int)Sizes.HERO_HEIGHT));
        }
        animation = new Animation(0.1f, keyFrames, Animation.PlayMode.LOOP);
        animationComponent.animations.put(StateEnum.RIGHT.ordinal(), animation);
        keyFrames.clear();

        for (int i = 1; i < 8; i++) {
            keyFrames.add(new TextureRegion(actorAtlas.findRegion(AssetConstants.HERO1_SPRITE), i * (int)Sizes.HERO_WIDTH, 3 * (int)Sizes.HERO_HEIGHT, (int)Sizes.HERO_WIDTH, (int)Sizes.HERO_HEIGHT));
        }
        animation = new Animation(0.1f, keyFrames, Animation.PlayMode.LOOP);
        animationComponent.animations.put(StateEnum.LEFT.ordinal(), animation);
        keyFrames.clear();

        for (int i = 0; i < 4; i++) {
            keyFrames.add(new TextureRegion(actorAtlas.findRegion(AssetConstants.HERO1_SPRITE), i * (int)Sizes.HERO_WIDTH, 0, (int)Sizes.HERO_WIDTH, (int)Sizes.HERO_HEIGHT));
        }
        animation = new Animation(0.1f, keyFrames, Animation.PlayMode.LOOP);
        animationComponent.animations.put(StateEnum.UP.ordinal(), animation);
        keyFrames.clear();

        for (int i = 1; i < 5; i++) {
            keyFrames.add(new TextureRegion(actorAtlas.findRegion(AssetConstants.HERO1_SPRITE), i * (int)Sizes.HERO_WIDTH, 1 * (int)Sizes.HERO_HEIGHT, (int)Sizes.HERO_WIDTH, (int)Sizes.HERO_HEIGHT));
        }
        animation = new Animation(0.1f, keyFrames, Animation.PlayMode.LOOP);
        animationComponent.animations.put(StateEnum.DOWN.ordinal(), animation);
        keyFrames.clear();

        for (int i = 0; i < 5; i++) {
            keyFrames.add(new TextureRegion(actorAtlas.findRegion(SOUL_REGION), i * SOUL_WIDTH, 0, SOUL_WIDTH, (int)Sizes.HERO_HEIGHT));
        }
        animation = new Animation(0.1f, keyFrames, Animation.PlayMode.NORMAL);
        animationComponent.animations.put(StateEnum.DIE.ordinal(), animation);
        keyFrames.clear();

        return animationComponent;
    }

    AnimationComponent buildPlayer2Animations(TextureAtlas actorAtlas) {
        AnimationComponent animationComponent = new AnimationComponent();
        Animation animation;
        Array<TextureRegion> keyFrames = new Array<>();
        keyFrames.clear();

        // move
        for (int i = 0; i < 4; i++) {
            int spriteX = i * (int)Sizes.HERO_WIDTH;
            int spriteY = (2*(int)Sizes.HERO_HEIGHT);
            keyFrames.add(new TextureRegion(actorAtlas.findRegion(AssetConstants.HERO2_SPRITE), spriteX, spriteY, (int)Sizes.HERO_WIDTH, (int)Sizes.HERO_HEIGHT));
        }
        animation = new Animation(0.2f, keyFrames, Animation.PlayMode.LOOP);
        animationComponent.animations.put(StateEnum.RIGHT.ordinal(), animation);
        keyFrames.clear();

        for (int i = 0; i < 4; i++) {
            int spriteX = i * (int)Sizes.HERO_WIDTH;
            int spriteY = (int)Sizes.HERO_HEIGHT;
            keyFrames.add(new TextureRegion(actorAtlas.findRegion(AssetConstants.HERO2_SPRITE), spriteX, spriteY, (int)Sizes.HERO_WIDTH, (int)Sizes.HERO_HEIGHT));
        }
        animation = new Animation(0.2f, keyFrames, Animation.PlayMode.LOOP);
        animationComponent.animations.put(StateEnum.LEFT.ordinal(), animation);
        keyFrames.clear();

        for (int i = 0; i < 4; i++) {
            int spriteX = i * (int)Sizes.HERO_WIDTH;
            int spriteY = (3*(int)Sizes.HERO_HEIGHT);
            keyFrames.add(new TextureRegion(actorAtlas.findRegion(AssetConstants.HERO2_SPRITE), spriteX, spriteY, (int)Sizes.HERO_WIDTH, (int)Sizes.HERO_HEIGHT));
        }
        animation = new Animation(0.2f, keyFrames, Animation.PlayMode.LOOP);
        animationComponent.animations.put(StateEnum.UP.ordinal(), animation);
        keyFrames.clear();

        for (int i = 0; i < 4; i++) {
            int spriteX = i * (int)Sizes.HERO_WIDTH;
            int spriteY = 0;
            keyFrames.add(new TextureRegion(actorAtlas.findRegion(AssetConstants.HERO2_SPRITE), spriteX, spriteY, (int)Sizes.HERO_WIDTH, (int)Sizes.HERO_HEIGHT));
        }
        animation = new Animation(0.2f, keyFrames, Animation.PlayMode.LOOP);
        animationComponent.animations.put(StateEnum.DOWN.ordinal(), animation);
        keyFrames.clear();

        for (int i = 0; i < 5; i++) {
            keyFrames.add(new TextureRegion(actorAtlas.findRegion(SOUL_REGION), i * SOUL_WIDTH, 0, SOUL_WIDTH, (int)Sizes.HERO_HEIGHT));
        }
        animation = new Animation(0.1f, keyFrames, Animation.PlayMode.NORMAL);
        animationComponent.animations.put(StateEnum.DIE.ordinal(), animation);
        keyFrames.clear();

        return animationComponent;
    }

    AnimationComponent buildPlayer3Animations(TextureAtlas actorAtlas) {
        AnimationComponent animationComponent = new AnimationComponent();
        Animation animation;
        Array<TextureRegion> keyFrames = new Array<>();
        keyFrames.clear();

        // move
        for (int i = 1; i < 6; i++) {
            int spriteX = i * (int)Sizes.HERO_WIDTH;
            int spriteY = (3*(int)Sizes.HERO_HEIGHT);
            keyFrames.add(new TextureRegion(actorAtlas.findRegion(AssetConstants.HERO3_SPRITE), spriteX, spriteY, (int)Sizes.HERO_WIDTH, (int)Sizes.HERO_HEIGHT));
        }
        animation = new Animation(0.2f, keyFrames, Animation.PlayMode.LOOP);
        animationComponent.animations.put(StateEnum.RIGHT.ordinal(), animation);
        keyFrames.clear();

        for (int i = 1; i < 6; i++) {
            int spriteX = i * (int)Sizes.HERO_WIDTH;
            int spriteY = (int)Sizes.HERO_HEIGHT;
            keyFrames.add(new TextureRegion(actorAtlas.findRegion(AssetConstants.HERO3_SPRITE), spriteX, spriteY, (int)Sizes.HERO_WIDTH, (int)Sizes.HERO_HEIGHT));
        }
        animation = new Animation(0.2f, keyFrames, Animation.PlayMode.LOOP);
        animationComponent.animations.put(StateEnum.LEFT.ordinal(), animation);
        keyFrames.clear();

        for (int i = 1; i < 6; i++) {
            int spriteX = i * (int)Sizes.HERO_WIDTH;
            int spriteY = (2*(int)Sizes.HERO_HEIGHT);
            keyFrames.add(new TextureRegion(actorAtlas.findRegion(AssetConstants.HERO3_SPRITE), spriteX, spriteY, (int)Sizes.HERO_WIDTH, (int)Sizes.HERO_HEIGHT));
        }
        animation = new Animation(0.2f, keyFrames, Animation.PlayMode.LOOP);
        animationComponent.animations.put(StateEnum.UP.ordinal(), animation);
        keyFrames.clear();

        for (int i = 1; i < 6; i++) {
            int spriteX = i * (int)Sizes.HERO_WIDTH;
            int spriteY = 0;
            keyFrames.add(new TextureRegion(actorAtlas.findRegion(AssetConstants.HERO3_SPRITE), spriteX, spriteY, (int)Sizes.HERO_WIDTH, (int)Sizes.HERO_HEIGHT));
        }
        animation = new Animation(0.2f, keyFrames, Animation.PlayMode.LOOP);
        animationComponent.animations.put(StateEnum.DOWN.ordinal(), animation);
        keyFrames.clear();

        for (int i = 0; i < 5; i++) {
            keyFrames.add(new TextureRegion(actorAtlas.findRegion(SOUL_REGION), i * SOUL_WIDTH, 0, SOUL_WIDTH, (int)Sizes.HERO_HEIGHT));
        }
        animation = new Animation(0.1f, keyFrames, Animation.PlayMode.NORMAL);
        animationComponent.animations.put(StateEnum.DIE.ordinal(), animation);
        keyFrames.clear();

        return animationComponent;
    }
}
