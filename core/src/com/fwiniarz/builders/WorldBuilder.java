package com.fwiniarz.builders;

import box2dLight.PointLight;
import box2dLight.RayHandler;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapLayers;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.QueryCallback;
import com.badlogic.gdx.physics.box2d.World;
import com.fwiniarz.ai.astar.AStarMap;
import com.fwiniarz.ai.astar.AStartPathFinding;
import com.fwiniarz.components.*;
import com.fwiniarz.constants.AssetConstants;
import com.fwiniarz.gamesys.GameManager;
import com.fwiniarz.utils.BuilderUtils;
import com.fwiniarz.utils.GlobalRandom;
import com.fwiniarz.utils.enums.StateEnum;

import java.util.List;
import java.util.ArrayList;

@SuppressWarnings({"PointlessArithmeticExpression", "UnnecessaryLocalVariable"})
public class WorldBuilder {

    private final TiledMap tiledMap;
    private final World world;
    private final RayHandler rayHandler;
    private final Engine engine;

    private final AssetManager assetManager;
    private final TextureAtlas actorAtlas;
    private final AnimationBuilder animationBuilder = new AnimationBuilder();

    private boolean wall;


    public WorldBuilder(TiledMap tiledMap, Engine engine, World world, RayHandler rayHandler) {
        this.tiledMap = tiledMap;
        this.engine = engine;
        this.world = world;
        this.rayHandler = rayHandler;

        assetManager = GameManager.instance.assetManager;
        actorAtlas = assetManager.get("core/assets/images/" + AssetConstants.SPRITE_PACK, TextureAtlas.class);
    }

    public void buildAll() {
        buildMap();
    }

    private void buildMap() {
        MapLayers mapLayers = tiledMap.getLayers();

        int mapWidth = ((TiledMapTileLayer) mapLayers.get(0)).getWidth();
        int mapHeight = ((TiledMapTileLayer) mapLayers.get(0)).getHeight();

        // walls
        MapLayer wallLayer = mapLayers.get("Wall"); // wall layer
        for (MapObject mapObject : wallLayer.getObjects()) {
            Rectangle rectangle = ((RectangleMapObject) mapObject).getRectangle();

            correctRectangle(rectangle);

            BodyDef bodyDef = new BodyDef();
            bodyDef.type = BodyDef.BodyType.StaticBody;
            bodyDef.position.set(rectangle.x + rectangle.width / 2, rectangle.y + rectangle.height / 2);

            Body body = world.createBody(bodyDef);
            PolygonShape polygonShape = new PolygonShape();
            polygonShape.setAsBox(rectangle.width / 2, rectangle.height / 2);
            FixtureDef fixtureDef = new FixtureDef();
            fixtureDef.shape = polygonShape;
            fixtureDef.filter.categoryBits = GameManager.WALL_BIT;
            fixtureDef.filter.maskBits = GameManager.PLAYER_BIT | GameManager.LIGHT_BIT;
            body.createFixture(fixtureDef);
            polygonShape.dispose();
        }

        // create map for A* path finding
        AStarMap aStarMap = new AStarMap(mapWidth, mapHeight);

        QueryCallback queryCallback = fixture -> {
            wall = fixture.getFilterData().categoryBits == GameManager.WALL_BIT;
            return false; // stop finding other fixtures in the query area
        };

        for (int y = 0; y < mapHeight; y++) {
            for (int x = 0; x < mapWidth; x++) {
                wall = false;
                world.QueryAABB(queryCallback, x + 0.2f, y + 0.2f, x + 0.8f, y + 0.8f);
                if (wall) {
                   aStarMap.getNodeAt(x, y).isWall = true;
                }
            }
        }
        GameManager.instance.pathfinder = new AStartPathFinding(aStarMap);

        // treasures
        MapLayer treasureLayer = mapLayers.get(AssetConstants.TREASURE_LAYER); // treasure layer layer
        int treasureId = 0;
        for (MapObject mapObject : treasureLayer.getObjects()) {
            Rectangle rec = ((RectangleMapObject) mapObject).getRectangle();
            correctRectangle(rec);

            float radius = 0.1f;
            TextureRegion textureRegion = new TextureRegion(actorAtlas.findRegion(AssetConstants.TREASURE_SPRITE), 0, 0, 48, 48);
            textureRegion.setRegionWidth(0);
            textureRegion.setRegionHeight(0);

            BodyDef bodyDef = new BodyDef();
            bodyDef.type = BodyDef.BodyType.DynamicBody;
            bodyDef.position.set(rec.x + rec.width / 2, rec.y + rec.height / 2);
            Body body = world.createBody(bodyDef);

            CircleShape circleShape = new CircleShape();
            circleShape.setRadius(radius);
            FixtureDef fixtureDef = new FixtureDef();
            fixtureDef.shape = circleShape;
            fixtureDef.filter.categoryBits = GameManager.TREASURE_BIT;
            fixtureDef.filter.maskBits = GameManager.PLAYER_BIT;
            fixtureDef.isSensor = true;
            body.createFixture(fixtureDef);

            circleShape.dispose();

            Entity entity = BuilderUtils.newEntity(new TreasureComponent(treasureId++),
                    new TransformComponent(rec.x + rec.width / 2, rec.y + rec.height / 2, 5),
                    new TextureComponent(textureRegion),
                    new MovementComponent(body));

            engine.addEntity(entity);
            body.setUserData(entity);
        }

        addPlayers(mapLayers);
    }

    // make rectangle correct position and dimensions
    private void correctRectangle(Rectangle rectangle) {
        rectangle.x = rectangle.x / GameManager.PPM;
        rectangle.y = rectangle.y / GameManager.PPM;
        rectangle.width = rectangle.width / GameManager.PPM;
        rectangle.height = rectangle.height / GameManager.PPM;
    }

    private void addPlayers(MapLayers layers)
    {
        if(GameManager.PLAYER_COUNT > 3) throw new IndexOutOfBoundsException("Max. number of players exceeded!");

        MapLayer playerLayer = layers.get(AssetConstants.HERO1_LAYER); // hero layer
        MapObjects playerLayerObjects = playerLayer.getObjects();
        List<Integer> playerSpawnIndices = findDistinctSpawnLocations(GameManager.PLAYER_COUNT, playerLayerObjects.getCount());

        int counter = 0;
        for(Integer spawnIndex : playerSpawnIndices)
        {
            PlayerComponent.Type playerType = (counter == 0 ? PlayerComponent.Type.PLAYER_1
                    : counter == 1 ? PlayerComponent.Type.PLAYER_2 : PlayerComponent.Type.PLAYER_3);
            Rectangle rectangle = ((RectangleMapObject) playerLayerObjects.get(spawnIndex)).getRectangle();
            correctRectangle(rectangle);
            createPlayer(rectangle.x + rectangle.width / 2, rectangle.y + rectangle.height / 2, playerType);
            counter++;
        }
    }

    private void createPlayer(float x, float y, PlayerComponent.Type playerType) {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(x, y);
        bodyDef.linearDamping = 64f;
        bodyDef.gravityScale = 10;

        Body body = world.createBody(bodyDef);

        CircleShape circleShape = new CircleShape();
        circleShape.setRadius(0.45f);
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = circleShape;
        fixtureDef.friction = 1f;
        fixtureDef.filter.categoryBits = GameManager.PLAYER_BIT;
        fixtureDef.filter.maskBits = GameManager.WALL_BIT | GameManager.TREASURE_BIT | GameManager.PLAYER_BIT;
        body.createFixture(fixtureDef);
        circleShape.dispose();

        // box2d light
        PointLight pointLight = new PointLight(rayHandler, 50, new Color(0.5f, 0.5f, 0.5f, 0.6f), 5f, 0, 0);
        pointLight.setContactFilter(GameManager.LIGHT_BIT, GameManager.NOTHING_BIT, GameManager.WALL_BIT);
        pointLight.setSoft(true);
        pointLight.setSoftnessLength(2.0f);
        pointLight.attachToBody(body);

        TextureRegion textureRegion = (playerType == PlayerComponent.Type.PLAYER_1 ? actorAtlas.findRegion(AssetConstants.HERO1_SPRITE)
                : playerType == PlayerComponent.Type.PLAYER_2 ? actorAtlas.findRegion(AssetConstants.HERO2_SPRITE) : actorAtlas.findRegion(AssetConstants.HERO3_SPRITE));

        PlayerComponent player = new PlayerComponent(body, playerType);

        Entity entity = new Entity();
        entity.add(player);
        entity.add(new TransformComponent(x, y, 1));
        entity.add(new MovementComponent(body));
        entity.add(new StateComponent(StateEnum.UP));
        entity.add(new TextureComponent(textureRegion));


        AnimationComponent animComponent = playerType == PlayerComponent.Type.PLAYER_1 ? animationBuilder.buildPlayer1Animations(actorAtlas)
                                                                                       : playerType == PlayerComponent.Type.PLAYER_2 ? animationBuilder.buildPlayer2Animations(actorAtlas)
                                                                                       : animationBuilder.buildPlayer3Animations(actorAtlas);
        entity.add(animComponent);

        engine.addEntity(entity);
        body.setUserData(entity);
    }

    private List<Integer> findDistinctSpawnLocations(int numberOfResults, int upperBound) {
        List<Integer> result = new ArrayList<>();

        if(numberOfResults > upperBound)
            return result;

        for(int i = 0; i < numberOfResults; i++) {
            int currentResult = -1;

            while(currentResult == -1 || result.contains(currentResult)) {
                currentResult = GlobalRandom.getInstance().nextInt(upperBound);
            }

            result.add(currentResult);
        }

        return result;
    }

    public List<PlayerComponent> getPlayers() {
        List<PlayerComponent> players = new ArrayList<>();
        ImmutableArray<Entity> entities = engine.getEntities();

        for(Entity entity : entities) {
            PlayerComponent component = entity.getComponent(PlayerComponent.class);
            if(component != null) {
                players.add(component);
            }
        }

        return players;
    }
}
