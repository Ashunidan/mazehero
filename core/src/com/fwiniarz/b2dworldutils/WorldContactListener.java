package com.fwiniarz.b2dworldutils;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.fwiniarz.components.TreasureComponent;
import com.fwiniarz.components.PlayerComponent;
import com.fwiniarz.gamesys.GameManager;

public class WorldContactListener implements ContactListener {

    private final ComponentMapper<TreasureComponent> treasureMapper = ComponentMapper.getFor(TreasureComponent.class);
    private final ComponentMapper<PlayerComponent> playerMapper = ComponentMapper.getFor(PlayerComponent.class);

    public WorldContactListener() {
    }

    @Override
    public void beginContact(Contact contact) {
        Fixture fixtureA = contact.getFixtureA();
        Fixture fixtureB = contact.getFixtureB();

        if(fixtureA.getFilterData().categoryBits == GameManager.PLAYER_BIT && fixtureB.getFilterData().categoryBits == GameManager.PLAYER_BIT) {
            PlayerComponent playerA = playerMapper.get((Entity) fixtureA.getBody().getUserData());
            PlayerComponent playerB = playerMapper.get((Entity) fixtureB.getBody().getUserData());
            if(!playerA.isFighting() && !playerB.isFighting()) {
                playerA.nearbyPlayer = playerB;
                playerB.nearbyPlayer = playerA;
            }
        }

        if (fixtureA.getFilterData().categoryBits == GameManager.TREASURE_BIT || fixtureB.getFilterData().categoryBits == GameManager.TREASURE_BIT) {
            if (fixtureA.getFilterData().categoryBits == GameManager.PLAYER_BIT) {
                Body body = fixtureB.getBody();
                Entity entity = (Entity) body.getUserData();
                TreasureComponent treasure = treasureMapper.get(entity);
                if(treasure.exists) {
                    treasure.taken = true;
                }
            } else if (fixtureB.getFilterData().categoryBits == GameManager.PLAYER_BIT) {
                Body body = fixtureA.getBody();
                Entity entity = (Entity) body.getUserData();
                TreasureComponent treasure = treasureMapper.get(entity);
                PlayerComponent player = playerMapper.get((Entity) fixtureB.getBody().getUserData());
                if(treasure.exists) {
                    treasure.taken = true;
                    treasure.takenBy = player.playerType;
                    System.out.println("Gained " + treasure.amount + " gold!");
                }
            }
        }
    }

    @Override
    public void endContact(Contact contact) {
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {
    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {
    }

}
