package com.fwiniarz.constants;

/**
 * MazeHero. Created by Filip on 22.07.2018.
 */
public class Sizes
{
    public static final float HERO_WIDTH = 32f;
    public static final float HERO_HEIGHT = 64f;
    public static final int TREASURE_SIZE = 48;
}
