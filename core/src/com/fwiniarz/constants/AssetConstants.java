package com.fwiniarz.constants;

/**
 * MazeHero. Created by Filip on 22.07.2018.
 */
public class AssetConstants
{
    public static final String HERO1_SPRITE = "Hero1";
    public static final String HERO2_SPRITE = "Hero2";
    public static final String HERO3_SPRITE = "Hero3";
    public static final String TREASURE_SPRITE = "Treasure";
    public static final String X_SPRITE = "Cross";
    public static final String SWORD_SPRITE = "Sword";

    public static final String HERO1_LAYER = "Hero1";
    public static final String TREASURE_LAYER = "Treasures";

    public static final String MAZE1 = "maze_small_64.tmx";
    public static final float MAZE1_WIDTH = 20f;
    public static final float MAZE1_HEIGHT = 20f;

    public static final String TEST_SPRITE_PACK = "actors-test.pack";
    public static final String SPRITE_PACK = "actors.pack";


}
