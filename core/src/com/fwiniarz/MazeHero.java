package com.fwiniarz;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.fwiniarz.gamesys.GameManager;
import com.fwiniarz.screens.PlayScreen;

public class MazeHero extends Game {
    
    public SpriteBatch batch;

    @Override
    public void create() {
        batch = new SpriteBatch();
        setScreen(new PlayScreen(this));
    }

    @Override
    public void render() {
        super.render();
    }

    @Override
    public void dispose() {
        batch.dispose();
        GameManager.instance.dispose();
    }
}
