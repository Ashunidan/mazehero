package com.fwiniarz.ai.fsm;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.RayCastCallback;
import com.badlogic.gdx.physics.box2d.World;
import com.fwiniarz.components.PlayerComponent;
import com.fwiniarz.entities.TreasureEntity;
import com.fwiniarz.gamesys.GameManager;
import com.fwiniarz.utils.Logger;
import com.fwiniarz.utils.MathUtil;
import com.fwiniarz.utils.enums.StateEnum;

import java.util.Iterator;

import static com.fwiniarz.utils.enums.StateEnum.UP;

class PlayerStateHelper {

    private RayCastCallback rayCastCallback;

    PlayerStateHelper(RayCastCallback callback) {
        rayCastCallback = callback;
    }

    StateEnum[] getDirectionChoices(PlayerAgent entity, StateEnum state) {
        Body body = entity.playerComponent.getBody();
        World world = body.getWorld();

        PlayerState.choicesList.clear();
        for (int i = 0; i < 4; i++) {
            PlayerState.choicesList.add(StateEnum.values()[i]);
        }

        if (state != null) PlayerState.choicesList.remove(state);

        PlayerState.tmpV1.set(body.getWorldCenter());

        Iterator<StateEnum> itor = PlayerState.choicesList.iterator();
        while (itor.hasNext()) {
            StateEnum current = itor.next();

            PlayerState.hitWall = false;
            switch (current) {
                case UP: // UP
                    PlayerState.tmpV2.set(PlayerState.tmpV1).add(0, PlayerState.RADIUS);
                    break;
                case DOWN: // DOWN
                    PlayerState.tmpV2.set(PlayerState.tmpV1).add(0, -PlayerState.RADIUS);
                    break;
                case LEFT: // LEFT
                    PlayerState.tmpV2.set(PlayerState.tmpV1).add(-PlayerState.RADIUS, 0);
                    break;
                case RIGHT: // RIGHT
                    PlayerState.tmpV2.set(PlayerState.tmpV1).add(PlayerState.RADIUS, 0);
                    break;
                default:
                    PlayerState.tmpV2.setZero();
                    break;
            }

            world.rayCast(rayCastCallback, PlayerState.tmpV1, PlayerState.tmpV2);
            if (PlayerState.hitWall) {
                itor.remove();
            }
        }

        if(state != null && PlayerState.choicesList.size() == 0) {
            PlayerState.choicesList.add(state);
        }

        StateEnum[] result = PlayerState.choicesList.toArray(new StateEnum[0]);

        return result;
    }

    StateEnum getRandomDirectionChoice(StateEnum[] choices) {
        if (choices.length == 0) {
            return UP;
        }
        int length = choices.length;

        StateEnum choice = choices[MathUtils.random(length - 1)];
        return choice;
    }

    boolean checkIfStuck(PlayerAgent entity) {
        boolean isStuck = false;

        entity.playerComponent.idleTimer += 0.01f;
        if(entity.playerComponent.idleTimer > 1f) {
            entity.playerComponent.idleTimer = 0;

            if(entity.playerComponent.lastPositionX == entity.getPosition().x && entity.playerComponent.lastPositionY == entity.getPosition().y) {
                isStuck = true;
            }

            entity.playerComponent.lastPositionX = entity.getPosition().x;
            entity.playerComponent.lastPositionY = entity.getPosition().y;
        }

        return isStuck;
    }

    Vector2 findNearestTreasure(PlayerAgent player) {
        Vector2 nearestTreasure = null;
        float smallestDistance = -1;

        if(GameManager.instance.getSpawnedTreasures() != null) {
            for(TreasureEntity treasure : GameManager.instance.getSpawnedTreasures()) {
                float distance = MathUtil.pointDistance(player.getPosition(), treasure.getPosition());

                if(smallestDistance == -1 || smallestDistance > distance) {
                    smallestDistance = distance;
                    nearestTreasure = treasure.getPosition();
                }
            }
        }

        return nearestTreasure;
    }

    boolean nearObject(PlayerAgent entity, Vector2 nearestTreasure, float distance) {
        if (nearestTreasure == null) {
            return false;
        }
        Vector2 pos = entity.getPosition();

        boolean nearTreasure = pos.dst2(nearestTreasure) < Math.pow(distance, 2);
        return nearTreasure;
    }


    Vector2 findNearestPlayer(PlayerAgent requestingPlayer) {
        Vector2 nearestPlayer = null;
        GameManager gmInst = GameManager.instance;
        PlayerComponent component = requestingPlayer.playerComponent;
        switch (requestingPlayer.playerComponent.playerType) {
            case PLAYER_1:
                float distance1to2 = MathUtil.pointDistance(component.getBody().getPosition(), gmInst.getPlayer2Position());
                float distance1to3 = MathUtil.pointDistance(component.getBody().getPosition(), gmInst.getPlayer3Position());
                nearestPlayer = (distance1to2 < distance1to3) ? gmInst.getPlayer2Position() : gmInst.getPlayer3Position();
                break;
            case PLAYER_2:
                float distance2to1 = MathUtil.pointDistance(component.getBody().getPosition(), gmInst.getPlayer1Position());
                float distance2to3 = MathUtil.pointDistance(component.getBody().getPosition(), gmInst.getPlayer3Position());
                nearestPlayer = (distance2to1 < distance2to3) ? gmInst.getPlayer1Position() : gmInst.getPlayer3Position();
                break;
            case PLAYER_3:
                float distance3to1 = MathUtil.pointDistance(component.getBody().getPosition(), gmInst.getPlayer1Position());
                float distance3to2 = MathUtil.pointDistance(component.getBody().getPosition(), gmInst.getPlayer2Position());
                nearestPlayer = (distance3to1 < distance3to2) ? gmInst.getPlayer1Position() : gmInst.getPlayer2Position();
                break;
        }

        return nearestPlayer;
    }

    boolean checkHitWall(PlayerAgent entity, StateEnum state) {
        Body body = entity.playerComponent.getBody();
        World world = body.getWorld();
        PlayerState.hitWall = false;

        PlayerState.tmpV1.set(body.getWorldCenter());

        switch (state) {
            case UP:
                PlayerState.tmpV2.set(PlayerState.tmpV1).add(0, PlayerState.RADIUS);
                break;
            case DOWN:
                PlayerState.tmpV2.set(PlayerState.tmpV1).add(0, -PlayerState.RADIUS);
                break;
            case LEFT:
                PlayerState.tmpV2.set(PlayerState.tmpV1).add(-PlayerState.RADIUS, 0);
                break;
            case RIGHT:
                PlayerState.tmpV2.set(PlayerState.tmpV1).add(PlayerState.RADIUS, 0);
                break;
            default:
                PlayerState.tmpV2.setZero();
                break;
        }
        world.rayCast(rayCastCallback, PlayerState.tmpV1, PlayerState.tmpV2);

        return PlayerState.hitWall;
    }

    boolean inPosition(PlayerAgent entity, float radius) {

        float x = entity.getPosition().x;
        float y = entity.getPosition().y;

        float xLow = MathUtils.floor(x) + 0.5f - radius;
        float xHight = MathUtils.floor(x) + 0.5f + radius;

        float yLow = MathUtils.floor(y) + 0.5f - radius;
        float yHight = MathUtils.floor(y) + 0.5f + radius;

        return xLow < x && x < xHight && yLow < y && y < yHight;
    }

    boolean checkDead(PlayerAgent entity) {
        return entity.playerComponent.hp <= 0;
    }

    boolean checkCanFight(PlayerAgent entity) {
        return entity.playerComponent.nearbyPlayer != null && entity.playerComponent.nearbyPlayer.fleeTimer <= 0;
    }
}
