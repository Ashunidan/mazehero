package com.fwiniarz.ai.fsm;

import com.badlogic.gdx.ai.fsm.State;
import com.badlogic.gdx.ai.msg.Telegram;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.RayCastCallback;
import com.fwiniarz.ai.astar.AStarMap;
import com.fwiniarz.ai.messaging.MHMessageManager;
import com.fwiniarz.ai.messaging.MsgTitle;
import com.fwiniarz.ai.messaging.telegrams.AttackMessage;
import com.fwiniarz.components.StateComponent;
import com.fwiniarz.entities.StateEntity;
import com.fwiniarz.gamesys.GameManager;
import com.fwiniarz.utils.Logger;
import com.fwiniarz.utils.PlayerUtils;
import com.fwiniarz.utils.enums.StateEnum;
import com.sun.istack.internal.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.fwiniarz.utils.enums.StateEnum.*;

@SuppressWarnings("UnnecessaryReturnStatement")
public enum PlayerState implements State<PlayerAgent> {
    MOVE_UP() {
        @Override
        public void update(PlayerAgent entity) {
            StateEntity stateEntity = commonStateActions(entity);
            if(stateEntity.shouldReturn) return;

            entity.playerComponent.setCurrentState(UP);

            Body body = entity.playerComponent.getBody();
            body.applyLinearImpulse(tmpV1.set(0, entity.playerComponent.speed).scl(body.getMass()), body.getWorldCenter(), true);

            if (body.getLinearVelocity().len2() > entity.playerComponent.speed * entity.playerComponent.speed) {
                Vector2 v = body.getLinearVelocity().scl(entity.playerComponent.speed / body.getLinearVelocity().len());
                body.setLinearVelocity(v);
            }

            if(stateEntity.isStuck) { pickRandomState(entity, null); return; }
            if (helper.checkHitWall(entity, UP)) { pickRandomState(entity, DOWN); return; }

            if (entity.timer > StateComponent.STATE_UPDATE_TIME && helper.inPosition(entity, 0.05f)) {
                entity.timer = 0;
                StateEnum newState = helper.getRandomDirectionChoice(helper.getDirectionChoices(entity, StateEnum.DOWN));
                if (newState != entity.playerComponent.getCurrentState()) {
                    changeState(entity, newState);
                    return;
                }
            }

            if(stateEntity.shouldFight) { return; }

            if (helper.nearObject(entity, helper.findNearestTreasure(entity), SPOT_TREASURE_RADIUS)
                    && helper.inPosition(entity, 0.1f)) {
                entity.stateMachine.changeState(LOOT_TREASURE);
            }
        }

    },
    MOVE_DOWN() {
        @Override
        public void update(PlayerAgent entity) {
            StateEntity stateEntity = commonStateActions(entity);
            if(stateEntity.shouldReturn) return;

            entity.playerComponent.setCurrentState(StateEnum.DOWN);

            Body body = entity.playerComponent.getBody();
            body.applyLinearImpulse(tmpV1.set(0, -entity.playerComponent.speed).scl(body.getMass()), body.getWorldCenter(), true);

            if (body.getLinearVelocity().len2() > entity.playerComponent.speed * entity.playerComponent.speed) {
                Vector2 v = body.getLinearVelocity().scl(entity.playerComponent.speed / body.getLinearVelocity().len());
                body.setLinearVelocity(v);
            }

            if(stateEntity.isStuck) { pickRandomState(entity, null); return; }

            if (helper.checkHitWall(entity, StateEnum.DOWN)) { pickRandomState(entity, UP); return; }

            if (entity.timer > StateComponent.STATE_UPDATE_TIME && helper.inPosition(entity, 0.05f)) {
                entity.timer = 0;
                StateEnum newState = helper.getRandomDirectionChoice(helper.getDirectionChoices(entity, UP));
                if (newState != entity.playerComponent.getCurrentState()) {
                    changeState(entity, newState);
                    return;
                }
            }

            if(stateEntity.shouldFight) { return; }

            if (helper.nearObject(entity, helper.findNearestTreasure(entity), SPOT_TREASURE_RADIUS)
                    && helper.inPosition(entity, 0.1f)) {
                entity.stateMachine.changeState(LOOT_TREASURE);
            }
        }
    },
    MOVE_LEFT() {
        @Override
        public void update(PlayerAgent entity) {
            StateEntity stateEntity = commonStateActions(entity);
            if(stateEntity.shouldReturn) return;

            entity.playerComponent.setCurrentState(StateEnum.LEFT);

            Body body = entity.playerComponent.getBody();
            body.applyLinearImpulse(tmpV1.set(-entity.playerComponent.speed, 0).scl(body.getMass()), body.getWorldCenter(), true);

            if (body.getLinearVelocity().len2() > entity.playerComponent.speed * entity.playerComponent.speed) {
                Vector2 v = body.getLinearVelocity().scl(entity.playerComponent.speed / body.getLinearVelocity().len());
                body.setLinearVelocity(v);
            }

            if(stateEntity.isStuck) { pickRandomState(entity, null); return; }

            if (helper.checkHitWall(entity, StateEnum.LEFT)) { pickRandomState(entity, RIGHT); return; }

            if (entity.timer > StateComponent.STATE_UPDATE_TIME && helper.inPosition(entity, 0.05f)) {
                entity.timer = 0;
                StateEnum newState = helper.getRandomDirectionChoice(helper.getDirectionChoices(entity, StateEnum.RIGHT));
                if (newState != entity.playerComponent.getCurrentState()) {
                    changeState(entity, newState);
                    return;
                }
            }

            if(stateEntity.shouldFight) return;

            if (helper.nearObject(entity, helper.findNearestTreasure(entity), SPOT_TREASURE_RADIUS)
                    && helper.inPosition(entity, 0.1f)) {
                entity.stateMachine.changeState(LOOT_TREASURE);
            }
        }
    },
    MOVE_RIGHT() {
        @Override
        public void update(PlayerAgent entity) {
            StateEntity stateEntity = commonStateActions(entity);
            if(stateEntity.shouldReturn) return;

            entity.playerComponent.setCurrentState(StateEnum.RIGHT);

            Body body = entity.playerComponent.getBody();
            body.applyLinearImpulse(tmpV1.set(entity.playerComponent.speed, 0).scl(body.getMass()), body.getWorldCenter(), true);

            if (body.getLinearVelocity().len2() > entity.playerComponent.speed * entity.playerComponent.speed) {
                Vector2 v = body.getLinearVelocity().scl(entity.playerComponent.speed / body.getLinearVelocity().len());
                body.setLinearVelocity(v);
            }

            if(stateEntity.isStuck) { pickRandomState(entity, null); return; }

            if (helper.checkHitWall(entity, StateEnum.RIGHT)) { pickRandomState(entity, LEFT); return; }

            if (entity.timer > StateComponent.STATE_UPDATE_TIME && helper.inPosition(entity, 0.05f)) {
                entity.timer = 0;
                StateEnum newState = helper.getRandomDirectionChoice(helper.getDirectionChoices(entity, StateEnum.LEFT));
                if (newState != entity.playerComponent.getCurrentState()) {
                    changeState(entity, newState);
                    return;
                }
            }

            if(stateEntity.shouldFight) return;

            if (helper.nearObject(entity, helper.findNearestTreasure(entity), SPOT_TREASURE_RADIUS)
                    && helper.inPosition(entity, 0.1f)) {
                entity.stateMachine.changeState(LOOT_TREASURE);
            }
        }
    },
    DIE() {
        @Override
        public void update(PlayerAgent entity) {
            entity.playerComponent.setCurrentState(StateEnum.DIE);
            changeState(entity, StateEnum.DIE);
        }

    },
    LOOT_TREASURE() {
        @Override
        public void update(PlayerAgent entity) {
            StateEntity stateEntity = basicCommonStateActions(entity);

            if(stateEntity.shouldReturn) return;

            if(helper.checkCanFight(entity)) { entity.stateMachine.changeState(FIGHTING); return; }

            Vector2 nearestTreasure = helper.findNearestTreasure(entity);
            if (nearestTreasure == null) { changeState(entity, StateEnum.values()[MathUtils.random(0, 3)]); return; }

            // do path finding every 0.1 second
            if (entity.nextNode == null || entity.timer > 0.1f) {
                entity.nextNode = GameManager.instance.pathfinder.findNextNode(entity.getPosition(), nearestTreasure);
                entity.timer = 0;
            }

            if (entity.nextNode == null) { changeState(entity, StateEnum.values()[MathUtils.random(0, 3)]); return; }

            float x = (entity.nextNode.x + 0.5f - entity.getPosition().x) * entity.playerComponent.speed;
            float y = (entity.nextNode.y + 0.5f - entity.getPosition().y) * entity.playerComponent.speed;
            int floorX = (int)Math.floor(x);
            int floorY = (int)Math.floor(y);

            Body body = entity.playerComponent.getBody();

            if (body.getLinearVelocity().isZero(0.1f) || helper.inPosition(entity, 0.5f)) {
                body.applyLinearImpulse(tmpV1.set(x, y).scl(body.getMass()), body.getWorldCenter(), true);
            }

            if(floorX > 1)
            {
                entity.playerComponent.setCurrentState(StateEnum.RIGHT);
            }
            else if(floorX >= -1)
            {
                if(floorY > 0)
                {
                    entity.playerComponent.setCurrentState(StateEnum.UP);
                }
                else
                {
                    entity.playerComponent.setCurrentState(StateEnum.DOWN);
                }
            }
            else
            {
                entity.playerComponent.setCurrentState(StateEnum.LEFT);
            }

            if (body.getLinearVelocity().len2() > entity.playerComponent.speed * entity.playerComponent.speed) {
                body.setLinearVelocity(body.getLinearVelocity().scl(entity.playerComponent.speed / body.getLinearVelocity().len()));
            }

            if (!helper.nearObject(entity, nearestTreasure, SPOT_TREASURE_RADIUS) && helper.inPosition(entity, 0.5f)) {
                changeState(entity, entity.playerComponent.getCurrentState());
                return;
            }
        }
    },
    FIGHTING() {
        @Override
        public void update(PlayerAgent entity) {
            StateEntity stateEntity = basicCommonStateActions(entity);

            if(stateEntity.shouldReturn) return;

            if(entity.playerComponent.nearbyPlayer == null) {
                pickRandomState(entity, null);
                return;
            }

            entity.playerComponent.hitTimer += 0.01f;

            if(entity.playerComponent.hitTimer > 1f) {
                entity.playerComponent.hitTimer = 0;
                MHMessageManager.attackMessage(entity, entity.playerComponent.nearbyPlayer.playerAgent, new AttackMessage(entity.playerComponent.hp, entity.playerComponent.damage));
            }

            if(entity.playerComponent.hitsTaken >= entity.playerComponent.hitEndurance) {
                entity.playerComponent.fleeTimer = 5f;
                StateEnum nearbyPlayerDirection = PlayerUtils.getDirectionToPlayer(entity.playerComponent, entity.playerComponent.nearbyPlayer);
                pickRandomStateWithPreference(entity, nearbyPlayerDirection.getOppositeDirection());
            }
        }
    };

    protected StateEntity commonStateActions(PlayerAgent entity) {
        StateEntity stateEntity = basicCommonStateActions(entity);
        entity.playerComponent.reduceFleeTimer(0.01f);

        if(stateEntity.shouldReturn) {
            return stateEntity;
        }

        stateEntity.isStuck = helper.checkIfStuck(entity);

        if(entity.playerComponent.nearbyPlayer != null) {
            entity.stateMachine.changeState(FIGHTING);
            stateEntity.shouldFight = true;
            return stateEntity;
        }

        return stateEntity;
    }

    protected StateEntity basicCommonStateActions(PlayerAgent entity) {
        StateEntity stateEntity = new StateEntity();

        if(GameManager.instance.isGameOver()) {
            stateEntity.shouldReturn = true;
            return stateEntity;
        }

        if(helper.checkDead(entity)) {
            entity.stateMachine.changeState(DIE);
            stateEntity.shouldReturn = true;
        }

        return stateEntity;
    }

    protected static RayCastCallback rayCastCallback = new RayCastCallback() {
        @Override
        public float reportRayFixture(Fixture fixture, Vector2 point, Vector2 normal, float fraction) {
            if (fixture.getFilterData().categoryBits == GameManager.WALL_BIT) {
                hitWall = true;
                return 0;
            }
            return 0;
        }
    };

    protected static final Vector2 tmpV1 = new Vector2();
    protected static final Vector2 tmpV2 = new Vector2();
    protected static boolean hitWall = false;
    protected static final float RADIUS = 0.55f;
    protected static final float SPOT_TREASURE_RADIUS = 5f;
    protected static final List<StateEnum> choicesList = new ArrayList<>(4);
    protected static final PlayerStateHelper helper = new PlayerStateHelper(rayCastCallback);

    @Override
    public void enter(PlayerAgent entity) {
        entity.timer = 0;
    }

    @Override
    public void exit(PlayerAgent entity) {
    }

    @Override
    public boolean onMessage(PlayerAgent entity, Telegram telegram) {
        return false;
    }

    protected void changeState(PlayerAgent entity, StateEnum state) {
        switch (state) {
            case UP: // UP
                entity.stateMachine.changeState(MOVE_UP);
                break;
            case DOWN: // DOWN
                entity.stateMachine.changeState(MOVE_DOWN);
                break;
            case LEFT: // LEFT
                entity.stateMachine.changeState(MOVE_LEFT);
                break;
            case RIGHT: // RIGHT
                entity.stateMachine.changeState(MOVE_RIGHT);
                break;
            case LOOT_TREASURE:
                entity.stateMachine.changeState(LOOT_TREASURE);
                break;
            case DIE: // DIE
                entity.stateMachine.changeState(DIE);
                break;
            case FIGHTING:
                entity.stateMachine.changeState(FIGHTING);
            default:
                break;
        }
    }

    protected void pickRandomState(PlayerAgent entity, @Nullable StateEnum defaultState) {
        if(defaultState == null) defaultState = StateEnum.DOWN;
        StateEnum newState = helper.getRandomDirectionChoice(helper.getDirectionChoices(entity, defaultState));
        changeState(entity, newState);
    }

    protected void pickRandomStateWithPreference(PlayerAgent entity, StateEnum preferredState) {
        StateEnum[] possibleStates = helper.getDirectionChoices(entity, null);
        if(Arrays.asList(possibleStates).contains(preferredState)) {
            changeState(entity, preferredState);
        } else {
            changeState(entity, helper.getRandomDirectionChoice(possibleStates));
        }
    }
}
