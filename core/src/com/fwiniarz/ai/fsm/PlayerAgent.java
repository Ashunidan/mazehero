package com.fwiniarz.ai.fsm;

import com.badlogic.gdx.ai.fsm.DefaultStateMachine;
import com.badlogic.gdx.ai.fsm.StateMachine;
import com.badlogic.gdx.ai.msg.Telegram;
import com.badlogic.gdx.ai.msg.Telegraph;
import com.badlogic.gdx.ai.utils.Location;
import com.badlogic.gdx.math.Vector2;
import com.fwiniarz.ai.astar.Node;
import com.fwiniarz.ai.messaging.MHMessageManager;
import com.fwiniarz.ai.messaging.MsgTitle;
import com.fwiniarz.ai.messaging.telegrams.AttackMessage;
import com.fwiniarz.components.PlayerComponent;
import com.fwiniarz.utils.Logger;

public class PlayerAgent implements Telegraph {

    public final PlayerComponent playerComponent;
    public final StateMachine<PlayerAgent, PlayerState> stateMachine;

    public float timer;

    public Node nextNode;

    public PlayerAgent(PlayerComponent playerComponent) {
        this.playerComponent = playerComponent;
        stateMachine = new DefaultStateMachine<>(this);
        timer = 0;
        MHMessageManager.getInstance().addListener(this, MsgTitle.ATTACK);
    }

    public Vector2 getPosition() {
        return playerComponent.getBody().getPosition();
    }

    public void update(float deltaTime) {
        timer += deltaTime;
        stateMachine.update();
    }

    @Override
    public boolean handleMessage(Telegram msg) {

        Logger.logLineStandard("message!");

        switch(msg.message) {
            case MsgTitle.ATTACK:
                handleAttackMessage(msg);
                break;
        }

        return stateMachine.handleMessage(msg);
    }

    private void handleAttackMessage(Telegram msg) {
        AttackMessage info = (AttackMessage)msg.extraInfo;
        playerComponent.hp = Math.max(0, playerComponent.hp - info.getAttackerDamage());
        playerComponent.hitsTaken += 1;
        playerComponent.updateHitEndurance(info.getAttackerHp(), info.getAttackerDamage());
    }
}
