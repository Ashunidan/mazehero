package com.fwiniarz.ai.messaging.telegrams;

public class AttackMessage {
    private int attackerHp;
    private int attackerDamage;

    public AttackMessage(int attackerHp, int attackerDamage) {
        this.attackerHp = attackerHp;
        this.attackerDamage = attackerDamage;
    }

    public int getAttackerHp() {
        return attackerHp;
    }

    public void setAttackerHp(int attackerHp) {
        this.attackerHp = attackerHp;
    }

    public int getAttackerDamage() {
        return attackerDamage;
    }

    public void setAttackerDamage(int attackerDamage) {
        this.attackerDamage = attackerDamage;
    }
}
