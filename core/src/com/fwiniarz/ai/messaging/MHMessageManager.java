package com.fwiniarz.ai.messaging;

import com.badlogic.gdx.ai.msg.MessageManager;
import com.badlogic.gdx.ai.msg.Telegraph;
import com.fwiniarz.ai.messaging.telegrams.AttackMessage;

public class MHMessageManager {

    public static MessageManager getInstance() {
        return MessageManager.getInstance();
    }

    public static void attackMessage(Telegraph from, Telegraph to, AttackMessage message) {
        getInstance().dispatchMessage(from, to, MsgTitle.ATTACK, message);
    }

}
