package com.fwiniarz.screens;

import box2dLight.RayHandler;
import com.badlogic.ashley.core.Engine;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.fwiniarz.MazeHero;
import com.fwiniarz.b2dworldutils.WorldContactListener;
import com.fwiniarz.builders.WorldBuilder;
import com.fwiniarz.components.PlayerComponent;
import com.fwiniarz.constants.AssetConstants;
import com.fwiniarz.gamesys.GameManager;
import com.fwiniarz.systems.AnimationSystem;
import com.fwiniarz.systems.MovementSystem;
import com.fwiniarz.systems.TreasureSystem;
import com.fwiniarz.systems.PlayerSystem;
import com.fwiniarz.systems.RenderSystem;
import com.fwiniarz.systems.StateSystem;

import com.badlogic.gdx.graphics.Color;
import com.fwiniarz.utils.PlayerUtils;

import java.util.List;

@SuppressWarnings("FieldCanBeLocal")
public class PlayScreen implements Screen {

    private final float WIDTH = AssetConstants.MAZE1_WIDTH;
    private final float HEIGHT = AssetConstants.MAZE1_HEIGHT + 2;

    private final MazeHero game;
    private SpriteBatch batch;

    private FitViewport viewport;
    private OrthographicCamera camera;

    private Engine engine;

    private AnimationSystem animationSystem;
    private MovementSystem movementSystem;
    private TreasureSystem treasureSystem;
    private PlayerSystem playerSystem;
    private RenderSystem renderSystem;
    private StateSystem stateSystem;

    private TiledMap tiledMap;
    private OrthogonalTiledMapRenderer tiledMapRenderer;

    private BitmapFont font;
    private FitViewport stageViewport;
    private Stage stage;

    private Label player1ScoreLabel;
    private Label player1HpLabel;
    private Label player1DamageLabel;
    private Label player2ScoreLabel;
    private Label player2HpLabel;
    private Label player2DamageLabel;
    private Label player3ScoreLabel;
    private Label player3HpLabel;
    private Label player3DamageLabel;
    private Label roundTimeLabel;

    private Label winnerLabel;
    private Label gameOverLabel;

    private StringBuilder stringBuilder;

    private World world;
    private Box2DDebugRenderer box2DDebugRenderer;
    private boolean showBox2DDebuggerRenderer;

    private Sprite player1XSprite;
    private Sprite player2XSprite;
    private Sprite player3XSprite;
    private Sprite player1SwordSprite;
    private Sprite player2SwordSprite;
    private Sprite player3SwordSprite;

    private RayHandler rayHandler;

    private float ambientLight = 0.5f;

    private boolean changeScreen;
    private float changeScreenCountDown = 2.0f;

    public PlayScreen(MazeHero game) {
        this.game = game;
        this.batch = game.batch;
    }

    @Override
    public void show() {
        camera = new OrthographicCamera();
        viewport = new FitViewport(WIDTH, HEIGHT, camera);
        camera.translate(WIDTH / 2, HEIGHT / 2);
        camera.update();

        batch = new SpriteBatch();

        playerSystem = new PlayerSystem();
        movementSystem = new MovementSystem();
        treasureSystem = new TreasureSystem();
        animationSystem = new AnimationSystem();
        renderSystem = new RenderSystem(batch);
        stateSystem = new StateSystem();

        engine = new Engine();
        engine.addSystem(playerSystem);
        engine.addSystem(treasureSystem);
        engine.addSystem(movementSystem);
        engine.addSystem(stateSystem);
        engine.addSystem(animationSystem);
        engine.addSystem(renderSystem);

        // box2d
        world = new World(Vector2.Zero, true);
        world.setContactListener(new WorldContactListener());
        box2DDebugRenderer = new Box2DDebugRenderer();
        showBox2DDebuggerRenderer = false;

        // box2d light
        rayHandler = new RayHandler(world);
        rayHandler.setAmbientLight(ambientLight);

        // load map
        tiledMap = new TmxMapLoader().load("core/assets/map/" + AssetConstants.MAZE1);
        tiledMapRenderer = new OrthogonalTiledMapRenderer(tiledMap, 1 / 64f, batch);

        GameManager.instance.worldBuilder = new WorldBuilder(tiledMap, engine, world, rayHandler);
        GameManager.instance.worldBuilder.buildAll();

        stageViewport = new FitViewport(WIDTH * 20, HEIGHT * 20);
        stage = new Stage(stageViewport, batch);

        drawLabels();

        prepareXSprites();
        prepareSwordSprites();

        stringBuilder = new StringBuilder();

        changeScreen = false;
    }

    private void prepareXSprites() {
        TextureAtlas textureAtlas = GameManager.instance.assetManager.get("core/assets/images/" + AssetConstants.SPRITE_PACK, TextureAtlas.class);
        player1XSprite = new Sprite(new TextureRegion(textureAtlas.findRegion(AssetConstants.X_SPRITE), 0, 0, 256, 128));
        player2XSprite = new Sprite(new TextureRegion(textureAtlas.findRegion(AssetConstants.X_SPRITE), 0, 0, 256, 128));
        player3XSprite = new Sprite(new TextureRegion(textureAtlas.findRegion(AssetConstants.X_SPRITE), 0, 0, 256, 128));
        player1XSprite.setBounds(0f, 0, 4, 2);
        player2XSprite.setBounds(0f, 0, 4, 2);
        player3XSprite.setBounds(0f, 0, 4, 2);
    }

    private void prepareSwordSprites() {
        TextureAtlas textureAtlas = GameManager.instance.assetManager.get("core/assets/images/" + AssetConstants.SPRITE_PACK, TextureAtlas.class);
        player1SwordSprite = new Sprite(new TextureRegion(textureAtlas.findRegion(AssetConstants.SWORD_SPRITE), 0, 0, 26, 64));
        player2SwordSprite = new Sprite(new TextureRegion(textureAtlas.findRegion(AssetConstants.SWORD_SPRITE), 0, 0, 26, 64));
        player3SwordSprite = new Sprite(new TextureRegion(textureAtlas.findRegion(AssetConstants.SWORD_SPRITE), 0, 0, 26, 64));
        player1SwordSprite.setBounds(0f, 0, 1, 1);
        player2SwordSprite.setBounds(0f, 0, 1, 1);
        player3SwordSprite.setBounds(0f, 0, 1, 1);
    }

    @SuppressWarnings("DanglingJavadoc")
    private void drawLabels() {
        font = new BitmapFont(Gdx.files.internal("core/assets/fonts/arial12.fnt"));
        Label.LabelStyle blueLabelStyle = new Label.LabelStyle(font, Color.CYAN);
        Label.LabelStyle greenLabelStyle = new Label.LabelStyle(font, Color.GREEN);
        Label.LabelStyle redLabelStyle = new Label.LabelStyle(font, Color.RED);

        /**
         * Player 1
         */
        if(GameManager.instance.getPlayer(PlayerComponent.Type.PLAYER_1) != null) {
            Label player1ScoreTextLabel = new Label("SCORE", blueLabelStyle);
            player1ScoreTextLabel.setPosition(WIDTH * 0.5f, HEIGHT * 19.25f);
            stage.addActor(player1ScoreTextLabel);

            player1ScoreLabel = new Label("0", blueLabelStyle);
            player1ScoreLabel.setPosition(WIDTH * 3f, HEIGHT * 19.25f);
            stage.addActor(player1ScoreLabel);

            Label player1HpTextLabel = new Label("HP", blueLabelStyle);
            player1HpTextLabel.setPosition(WIDTH * 0.5f, HEIGHT * 18.75f);
            stage.addActor(player1HpTextLabel);

            player1HpLabel = new Label("100", blueLabelStyle);
            player1HpLabel.setPosition(WIDTH * 3f, HEIGHT * 18.75f);
            stage.addActor(player1HpLabel);

            Label player1DamageTextLabel = new Label("DMG", blueLabelStyle);
            player1DamageTextLabel.setPosition(WIDTH * 0.5f, HEIGHT * 18.25f);
            stage.addActor(player1DamageTextLabel);

            player1DamageLabel = new Label("" + GameManager.instance.getPlayer(PlayerComponent.Type.PLAYER_1).damage, blueLabelStyle);
            player1DamageLabel.setPosition(WIDTH * 3f, HEIGHT * 18.25f);
            stage.addActor(player1DamageLabel);
        }

        /**
         * Player 2
         */
        if(GameManager.instance.getPlayer(PlayerComponent.Type.PLAYER_2) != null) {
            Label player2ScoreTextLabel = new Label("SCORE", greenLabelStyle);
            player2ScoreTextLabel.setPosition(WIDTH * 7.5f, HEIGHT * 19.25f);
            stage.addActor(player2ScoreTextLabel);

            player2ScoreLabel = new Label("0", greenLabelStyle);
            player2ScoreLabel.setPosition(WIDTH * 10f, HEIGHT * 19.25f);
            stage.addActor(player2ScoreLabel);

            Label player2HpTextLabel = new Label("HP", greenLabelStyle);
            player2HpTextLabel.setPosition(WIDTH * 7.5f, HEIGHT * 18.75f);
            stage.addActor(player2HpTextLabel);

            player2HpLabel = new Label("100", greenLabelStyle);
            player2HpLabel.setPosition(WIDTH * 10f, HEIGHT * 18.75f);
            stage.addActor(player2HpLabel);

            Label player2DamageTextLabel = new Label("DMG", greenLabelStyle);
            player2DamageTextLabel.setPosition(WIDTH * 7.5f, HEIGHT * 18.25f);
            stage.addActor(player2DamageTextLabel);

            player2DamageLabel = new Label("" + GameManager.instance.getPlayer(PlayerComponent.Type.PLAYER_2).damage, greenLabelStyle);
            player2DamageLabel.setPosition(WIDTH * 10f, HEIGHT * 18.25f);
            stage.addActor(player2DamageLabel);
        }

        /**
         * Player 3
         */
        if(GameManager.instance.getPlayer(PlayerComponent.Type.PLAYER_3) != null) {
            Label player3ScoreTextLabel = new Label("SCORE", redLabelStyle);
            player3ScoreTextLabel.setPosition(WIDTH * 14.5f, HEIGHT * 19.25f);
            stage.addActor(player3ScoreTextLabel);

            player3ScoreLabel = new Label("0", redLabelStyle);
            player3ScoreLabel.setPosition(WIDTH * 17f, HEIGHT * 19.25f);
            stage.addActor(player3ScoreLabel);

            Label player3HpTextLabel = new Label("HP", redLabelStyle);
            player3HpTextLabel.setPosition(WIDTH * 14.5f, HEIGHT * 18.75f);
            stage.addActor(player3HpTextLabel);

            player3HpLabel = new Label("100", redLabelStyle);
            player3HpLabel.setPosition(WIDTH * 17f, HEIGHT * 18.75f);
            stage.addActor(player3HpLabel);

            Label player3DamageTextLabel = new Label("DMG", redLabelStyle);
            player3DamageTextLabel.setPosition(WIDTH * 14.5f, HEIGHT * 18.25f);
            stage.addActor(player3DamageTextLabel);

            player3DamageLabel = new Label("" + GameManager.instance.getPlayer(PlayerComponent.Type.PLAYER_3).damage, redLabelStyle);
            player3DamageLabel.setPosition(WIDTH * 17f, HEIGHT * 18.25f);
            stage.addActor(player3DamageLabel);
        }
    }

    private void handleInput() {
        if (Gdx.input.isKeyJustPressed(Input.Keys.B)) {
            showBox2DDebuggerRenderer = !showBox2DDebuggerRenderer;
        }

        if (GameManager.instance.isGameOver() && Gdx.input.isKeyJustPressed(Input.Keys.ENTER)) {
            changeScreen = true;
        }

        if(Gdx.input.isKeyJustPressed(Input.Keys.Q)) {
            System.exit(0);
        }
    }

    @Override
    public void render(float delta) {
        handleInput();
        List<PlayerComponent> players = GameManager.instance.getPlayers();

        Gdx.gl.glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        tiledMapRenderer.setView(camera);
        tiledMapRenderer.render();

        batch.setProjectionMatrix(camera.combined);
        if (changeScreen) {
            playerSystem.setProcessing(false);
            movementSystem.setProcessing(false);
        } else {
            world.step(1 / 60f, 8, 3);
        }
        engine.update(delta);

        rayHandler.setCombinedMatrix(camera);
        rayHandler.updateAndRender();

        if (showBox2DDebuggerRenderer) {
            box2DDebugRenderer.render(world, camera.combined);
        }

        updateScore(delta);
        updateHpLabels(players);
        updateTimeLabel();
        drawXLabelsForDeadPlayers();
        drawSwordSpritesForFightingPlayers(players);
        checkGameOverConditions(players);

        stage.draw();

        if (changeScreen) {
            changeScreenCountDown -= delta;
            if (changeScreenCountDown <= 1) {
                // fade out effect
                ambientLight -= delta;
                rayHandler.setAmbientLight(MathUtils.clamp(ambientLight, 0f, 1f));
                rayHandler.removeAll();
            }

            if (changeScreenCountDown <= 0) {
                GameManager.instance.resetGame();
                game.setScreen(new PlayScreen(game));
            }
        }
    }

    private void updateScore(float delta) {
        stringBuilder.setLength(0);

        if (GameManager.instance.player1DisplayScore < GameManager.instance.player1Score) {
            GameManager.instance.player1DisplayScore = Math.min(GameManager.instance.player1Score, GameManager.instance.player1DisplayScore + (int) (600 * delta));
        }

        if (GameManager.instance.player2DisplayScore < GameManager.instance.player2Score) {
            GameManager.instance.player2DisplayScore = Math.min(GameManager.instance.player2Score, GameManager.instance.player2DisplayScore + (int) (600 * delta));
        }

        if (GameManager.instance.player3DisplayScore < GameManager.instance.player3Score) {
            GameManager.instance.player3DisplayScore = Math.min(GameManager.instance.player3Score, GameManager.instance.player3DisplayScore + (int) (600 * delta));
        }

        if(player1ScoreLabel != null) {
            player1ScoreLabel.setText(stringBuilder.append(GameManager.instance.player1DisplayScore));
            stringBuilder.setLength(0);
        }

        if(player2ScoreLabel != null) {
            player2ScoreLabel.setText(stringBuilder.append(GameManager.instance.player2DisplayScore));
            stringBuilder.setLength(0);
        }

        if(player3ScoreLabel != null) {
            player3ScoreLabel.setText(stringBuilder.append(GameManager.instance.player3DisplayScore));
            stringBuilder.setLength(0);
        }
    }

    private void updateHpLabels(List<PlayerComponent> players) {

        PlayerComponent player1 = PlayerUtils.findPlayerByType(players, PlayerComponent.Type.PLAYER_1);
        if(player1HpLabel != null) player1HpLabel.setText(player1 == null ? "0" : "" + player1.hp);

        PlayerComponent player2 = PlayerUtils.findPlayerByType(players, PlayerComponent.Type.PLAYER_2);
        if(player2HpLabel != null) player2HpLabel.setText(player2 == null ? "0" : "" + player2.hp);

        PlayerComponent player3 = PlayerUtils.findPlayerByType(players, PlayerComponent.Type.PLAYER_3);
        if(player3HpLabel != null) player3HpLabel.setText(player3 == null ? "0" : "" + player3.hp);
    }

    private void updateTimeLabel() {
        if(!GameManager.instance.isGameOver()) {
            if(roundTimeLabel == null) {
                Label.LabelStyle labelStyle = new Label.LabelStyle(font, Color.WHITE);
                GameManager.instance.gameStartTimestamp = System.currentTimeMillis();
                roundTimeLabel = new Label("Time remaining: 5:00", labelStyle);
                roundTimeLabel.setPosition(WIDTH * 7f, HEIGHT * 17.5f);
                stage.addActor(roundTimeLabel);
            }

            long elapsedTimestamp = System.currentTimeMillis() - GameManager.instance.gameStartTimestamp;
            GameManager.instance.gameElapsedTimestamp = elapsedTimestamp;

            int seconds = (int)(GameManager.ROUND_TIME_MS - elapsedTimestamp) / 1000;
            int displayMinutes = seconds / 60;
            int displaySeconds = seconds % 60;
            String displayMinutesString = "" + displayMinutes;
            String displaySecondsString = (displaySeconds < 10) ? "0" + displaySeconds : "" + displaySeconds;

            roundTimeLabel.setText("Time remaining: " + displayMinutesString + ":" + displaySecondsString);
        }
    }

    private void drawXLabelsForDeadPlayers() {
        GameManager gm = GameManager.instance;
        if(player1ScoreLabel != null && !gm.player1Alive) {
            batch.begin();
            player1XSprite.setPosition(0, 1 * WIDTH);
            player1XSprite.draw(batch);
            batch.end();
        }

        if(player2ScoreLabel != null && !gm.player2Alive) {
            batch.begin();
            player1XSprite.setPosition(7, 1 * WIDTH);
            player1XSprite.draw(batch);
            batch.end();
        }

        if(player3ScoreLabel != null && !gm.player3Alive) {
            batch.begin();
            player1XSprite.setPosition(14, 1* WIDTH);
            player1XSprite.draw(batch);
            batch.end();
        }

    }

    private void drawSwordSpritesForFightingPlayers(List<PlayerComponent> players) {
        for(PlayerComponent player : players) {
            switch (player.playerType) {
                case PLAYER_1:
                    batch.begin();
                    player1SwordSprite.setPosition(4, HEIGHT - 1.75f);
                    player1SwordSprite.draw(batch);
                    player1SwordSprite.setAlpha(player.isFighting() ? 1f : 0f);
                    batch.end();
                    break;
                case PLAYER_2:
                    batch.begin();
                    player2SwordSprite.setPosition(11, HEIGHT - 1.75f);
                    player2SwordSprite.draw(batch);
                    player2SwordSprite.setAlpha(player.isFighting() ? 1f : 0f);
                    batch.end();
                    break;
                case PLAYER_3:
                    batch.begin();
                    player3SwordSprite.setPosition(18, HEIGHT - 1.75f);
                    player3SwordSprite.draw(batch);
                    player3SwordSprite.setAlpha(player.isFighting() ? 1f : 0f);
                    batch.end();
                    break;
            }
        }
    }

    private void checkGameOverConditions(List<PlayerComponent> players) {
        if((GameManager.PLAYER_COUNT != 1 && players.size() <= 1)
                || GameManager.instance.gameElapsedTimestamp > GameManager.ROUND_TIME_MS - 200) {
            GameManager.instance.makeGameOver();
        }

        if (GameManager.instance.isGameOver() && !changeScreen) {
            showGameOverMessage(true);
        } else {
            showGameOverMessage(false);
        }
    }

    private void showGameOverMessage(boolean show) {
        PlayerComponent.Type winner;
        Label.LabelStyle winnerStyle;
        Label.LabelStyle whiteStyle = new Label.LabelStyle(font, Color.WHITE);

        if(show) {
            winner = GameManager.instance.getWinner();
            winnerStyle = new Label.LabelStyle(font, getWinnerLabelColor(winner));
        } else {
            winner = null;
            winnerStyle = whiteStyle;
        }

        String winnerLabelText = (winner == null) ? "No winner!" : winner.name().replaceAll("_", " ") + " won!";
        winnerLabel = new Label(winnerLabelText, winnerStyle);
        winnerLabel.setPosition(WIDTH * 8f, HEIGHT * 11.5f);
        gameOverLabel = new Label("Press Enter to continue\nPress Q to quit", whiteStyle);
        gameOverLabel.setPosition(WIDTH * 7f, HEIGHT * 10f);

        winnerLabel.setVisible(show);
        gameOverLabel.setVisible(show);

        stage.addActor(gameOverLabel);
        stage.addActor(winnerLabel);
    }

    private Color getWinnerLabelColor(PlayerComponent.Type player) {
        Color color = Color.WHITE;
        if(player != null) {
            switch (player) {
                case PLAYER_1:
                    color = Color.CYAN;
                    break;
                case PLAYER_2:
                    color = Color.GREEN;
                    break;
                case PLAYER_3:
                    color = Color.RED;
                    break;
            }
        }

        return color;
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
        stageViewport.update(width, height);
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void hide() {
        dispose();
    }

    @Override
    public void dispose() {
        font.dispose();
        stage.dispose();
        rayHandler.dispose();
        tiledMap.dispose();
        tiledMapRenderer.dispose();
        world.dispose();
        box2DDebugRenderer.dispose();
    }

}
