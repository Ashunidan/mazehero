package com.fwiniarz.entities;

import com.badlogic.gdx.math.Vector2;
import com.fwiniarz.components.TreasureComponent;

/**
 * MazeHero. Created by Filip on 01.09.2018.
 */
public class TreasureEntity {
    private Vector2 position;
    private TreasureComponent treasure;

    public TreasureEntity(TreasureComponent treasure, Vector2 position) {
        this.position = position;
        this.treasure = treasure;
    }

    public Vector2 getPosition() {
        return position;
    }

    public void setPosition(Vector2 position) {
        this.position = position;
    }

    public TreasureComponent getTreasure() {
        return treasure;
    }

    public void setTreasure(TreasureComponent treasure) {
        this.treasure = treasure;
    }
}
