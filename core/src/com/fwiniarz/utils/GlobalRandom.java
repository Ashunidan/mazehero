package com.fwiniarz.utils;

import java.util.Random;

/**
 * MazeHero. Created by Filip on 09.09.2018.
 */
public class GlobalRandom {
    private static final Random instance = new Random(System.currentTimeMillis());

    public static Random getInstance() {
        return instance;
    }
}
