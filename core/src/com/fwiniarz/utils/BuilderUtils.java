package com.fwiniarz.utils;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;

/**
 * MazeHero. Created by Filip on 23.07.2018.
 */
public class BuilderUtils
{
    public static Entity newEntity(Component... components) {
        Entity entity = new Entity();
        for(Component component : components) {
            entity.add(component);
        }

        return entity;
    }
}
