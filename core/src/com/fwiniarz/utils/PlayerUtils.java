package com.fwiniarz.utils;

import com.badlogic.gdx.math.Vector2;
import com.fwiniarz.components.PlayerComponent;
import com.fwiniarz.utils.enums.StateEnum;

import java.util.List;

public class PlayerUtils {

    public static PlayerComponent findPlayerByType(List<PlayerComponent> players, PlayerComponent.Type type) {
        PlayerComponent result = null;
        for(PlayerComponent player : players) {
            if(player.playerType == type) {
                result = player;
                break;
            }
        }

        return result;
    }

    public static StateEnum getDirectionToPlayer(PlayerComponent from, PlayerComponent to) {
        StateEnum direction = null;
        Vector2 pos1 = from.getBody().getPosition();
        Vector2 pos2 = to.getBody().getPosition();
        float x1 = MathUtil.roundToHalf(pos1.x);
        float x2 = MathUtil.roundToHalf(pos2.x);
        float y1 = MathUtil.roundToHalf(pos1.y);
        float y2 = MathUtil.roundToHalf(pos2.y);

        if((x2 > x1 && y2 > y1) || (x2 == x1 && y2 > y1) || (x2 < x1 && y2 > y1)) {
            direction = StateEnum.UP;
        } else if(x2 > x1 && y2 == y1) {
            direction = StateEnum.RIGHT;
        } else if((x2 > x1 && y2 < y1) || (x2 == x1 && y2 < y1) || (x2 < x1 && y2 < y1)) {
            direction = StateEnum.DOWN;
        }  else if(x2 < x1 && y2 == y1) {
            direction = StateEnum.LEFT;
        }

        return direction;
    }
}
