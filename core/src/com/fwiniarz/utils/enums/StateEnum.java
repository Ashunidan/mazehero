package com.fwiniarz.utils.enums;

import com.fwiniarz.ai.fsm.PlayerState;

/**
 * MazeHero. Created by Filip on 29.07.2018.
 */
public enum StateEnum {
    UP,
    DOWN,
    LEFT,
    RIGHT,
    DIE,
    LOOT_TREASURE,
    FIGHTING;

    public PlayerState toPlayerState() {
        PlayerState state = null;
        switch (this) {
            case UP:
                state = PlayerState.MOVE_UP;
                break;
            case DOWN:
                state = PlayerState.MOVE_DOWN;
                break;
            case LEFT:
                state = PlayerState.MOVE_LEFT;
                break;
            case RIGHT:
                state = PlayerState.MOVE_RIGHT;
                break;
            case DIE:
                state = PlayerState.DIE;
                break;
            case LOOT_TREASURE:
                state = PlayerState.LOOT_TREASURE;
                break;
            case FIGHTING:
                state = PlayerState.FIGHTING;
                break;
        }

        return state;
    }

    public StateEnum getOppositeDirection() {
        StateEnum result = this;
        switch (this) {
            case UP:
                result = DOWN;
                break;
            case DOWN:
                result = UP;
                break;
            case LEFT:
                result = RIGHT;
                break;
            case RIGHT:
                result = LEFT;
                break;
        }

        return result;
    }
}
