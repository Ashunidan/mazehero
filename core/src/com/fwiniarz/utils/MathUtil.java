package com.fwiniarz.utils;

import com.badlogic.gdx.math.Vector2;

public class MathUtil {

    public static float pointDistance(Vector2 point1, Vector2 point2) {
        float x1 = point1.x;
        float x2 = point2.x;
        float y1 = point1.y;
        float y2 = point2.y;
        float distance = (float)Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
        return distance;
    }

    public static float roundToHalf(float value) {
        float result = Math.round(value * 2) / 2.0f;
        return result;
    }
}
