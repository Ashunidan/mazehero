package com.fwiniarz.utils;

import java.text.DecimalFormat;

/**
 * MazeHero. Created by Filip on 01.09.2018.
 */
public class Logger
{
    private static final DecimalFormat df = new DecimalFormat("#.##");

    public static void logLineImportant(String message) {
        System.out.println(makeTimestamp() + "-> " + message);
    }

    public static void logLineStandard(String message) {
        System.out.println(makeTimestamp() + "-> " + message);
    }

    public static void logLineMinor(String message) {
        System.out.println(makeTimestamp() + "-> " + message);
    }

    public static void logLineDebug(String message) {
        System.out.println(makeTimestamp() + "-> " + message);
    }

    public static void logLineTrash(String message) {
        System.out.println(makeTimestamp() + "-> " + message);
    }

    public static String printRounded(float value) {
        return df.format(value);
    }

    private static String makeTimestamp() {
        return System.currentTimeMillis() + "";
    }
}
