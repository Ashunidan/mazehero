package com.fwiniarz.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.fwiniarz.constants.Sizes;

public class TextureComponent implements Component {
    
    public TextureRegion region;

    public TextureComponent(TextureRegion textureRegion) {
        region = new TextureRegion(textureRegion);
    }

    public void show(boolean show) {
        region.setRegionWidth(show ? Sizes.TREASURE_SIZE : 0);
        region.setRegionHeight(show ? Sizes.TREASURE_SIZE : 0);
    }
}
