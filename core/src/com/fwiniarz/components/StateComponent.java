package com.fwiniarz.components;

import com.badlogic.ashley.core.Component;
import com.fwiniarz.utils.enums.StateEnum;

public class StateComponent implements Component {
    public static final float STATE_UPDATE_TIME = 0.2f;
    private float stateTime;
    private StateEnum state;
    
    public StateComponent() {
        this(StateEnum.UP);
    }
    
    public StateComponent(StateEnum state) {
        this.state = state;
        stateTime = 0;
    }
    
    public void increaseStateTime(float delta) {
        stateTime += delta;
    }
    
    public float getStateTime() {
        return stateTime;
    }
    
    public void resetStateTime() {
        stateTime = 0;
    }
    
    public StateEnum getState() {
        return state;
    }
    
    public void setState(StateEnum newState) {
        if (state == newState) {
            return;
        }
        state = newState;
        stateTime = 0;
    }
}
