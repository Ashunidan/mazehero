package com.fwiniarz.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.fwiniarz.ai.PlayerAI;
import com.fwiniarz.ai.fsm.PlayerAgent;
import com.fwiniarz.ai.fsm.PlayerState;
import com.fwiniarz.gamesys.GameManager;
import com.fwiniarz.utils.GlobalRandom;
import com.fwiniarz.utils.Logger;
import com.fwiniarz.utils.enums.StateEnum;

import java.util.Objects;

public class PlayerComponent implements Component {
    public PlayerAI ai;
    public PlayerAgent playerAgent;

    private final Body body;

    private StateEnum currentState;
    private int maxHp;
    public int hp;
    public int damage;
    public float speed;
    public int hitEndurance;
    public Type playerType;

    public float idleTimer;
    public float lastPositionX;
    public float lastPositionY;
    public PlayerComponent nearbyPlayer;

    public float hitTimer;
    public int hitsTaken;
    public float fleeTimer;

    private float hpRegenTimer;

    public PlayerComponent(Body body, Type playerType) {
        this.body = body;
        ai = new PlayerAI(body);
        playerAgent = new PlayerAgent(this);
        playerAgent.stateMachine.setInitialState(PlayerState.MOVE_UP);
        this.playerType = playerType;
        markPlayerAlive();

        hp = 100;
        damage = 10;
        speed = 4;
        hitEndurance = 3;

        distributeStatPoints(10);

        maxHp = hp;
    }

    public Body getBody() {
        return body;
    }

    public enum Type {
        PLAYER_1,
        PLAYER_2,
        PLAYER_3
    }

    public StateEnum getCurrentState() {
        return currentState;
    }

    public void setCurrentState(StateEnum currentState) {
        this.currentState = currentState;
    }

    private void distributeStatPoints(int statPoints) {
        int hpDistributed = 0;
        int damageDistributed = 0;
        int speedDistributed = 0;
        for(int i = 0; i < statPoints; i++) {
            int choice = GlobalRandom.getInstance().nextInt(3);
            switch (choice) {
                case 0:
                    hpDistributed++;
                    hp += 10;
                    break;
                case 1:
                    damageDistributed++;
                    damage += 1;
                    break;
                case 2:
                    speedDistributed++;
                    speed += 0.25f;
                    break;
            }
        }

        Logger.logLineImportant(playerType.name() + " points distribution -> hp: " + hpDistributed + ", damage: " + damageDistributed + ", speed: " + speedDistributed);
    }

    public boolean isFighting() {
        boolean isFighting = (nearbyPlayer != null);
        return isFighting;
    }

    public void reduceFleeTimer(float delta) {
        fleeTimer -= delta;
        if(fleeTimer <= 0) {
            fleeTimer = 0;
            hitsTaken = 0;
        }
    }

    public void regenerateHp(float deltaTime) {
        if(!isFighting()) {
            hpRegenTimer += deltaTime;
            if(hpRegenTimer >= 4.0f) {
                hpRegenTimer = 0;
                if(hp < maxHp) {
                    hp++;
                }
            }

        }
    }

    public void updateHitEndurance(int attackerHp, int attackerDmg) {
        int minHitEndurance = 3;
        int resultHitEndurance = 5;
        if(nearbyPlayer != null) {
            resultHitEndurance += 0.5 * (((hp - attackerHp) / 10) + (damage - attackerDmg));
        }

        if(resultHitEndurance < minHitEndurance) {
            resultHitEndurance = minHitEndurance;
        }

        hitEndurance = resultHitEndurance;
    }

    public void markPlayerDead() {
        switch (playerType) {
            case PLAYER_1:
                GameManager.instance.player1Alive = false;
                break;
            case PLAYER_2:
                GameManager.instance.player2Alive = false;
                break;
            case PLAYER_3:
                GameManager.instance.player3Alive = false;
                break;
        }
    }

    public void markPlayerAlive() {
        switch (playerType) {
            case PLAYER_1:
                GameManager.instance.player1Alive = true;
                break;
            case PLAYER_2:
                GameManager.instance.player2Alive = true;
                break;
            case PLAYER_3:
                GameManager.instance.player3Alive = true;
                break;
        }
    }

//    public void retreat() {
//        if(nearbyPlayer != null) {
//            nearbyPlayer = null;
//        }
//    }
}
