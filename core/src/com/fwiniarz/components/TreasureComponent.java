package com.fwiniarz.components;

import com.badlogic.ashley.core.Component;
import com.fwiniarz.gamesys.GameManager;

public class TreasureComponent implements Component {

    public int id;
    public PlayerComponent.Type takenBy;
    public boolean exists;
    public boolean taken;
    public int amount;

    public TreasureComponent(int id) {
        this.id = id;
    }

    public void collect(PlayerComponent.Type player) {
        GameManager.instance.removeSpawnedTreasure(this);
        exists = false;
        taken = true;
        takenBy = player;
    }

    public void spawn(int amount) {
        exists = true;
        taken = false;
        takenBy = null;
        this.amount = amount;
    }
}
